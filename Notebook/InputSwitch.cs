﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Notebook
{
    class InputSwitch
    {
        public static void GetInput(int input)
        {
            NotePad pad = new NotePad();
            int i,id;
            string title, content;
            switch (input)
            {
                case 1:
                    Console.WriteLine("Selected: Add new note");
                    Console.WriteLine("Title:");
                    title = Console.ReadLine();
                    Console.WriteLine("Content:");
                    content = Console.ReadLine();
                    pad.AddNote(title, content);
                    break;
                case 2:
                    Console.WriteLine("Selected: Delete a note");
                    i = 0;
                    foreach (Note note in NoteMemory.Instance.noteList)
                    {
                        Console.WriteLine(i + " " + note.title);
                        i++;
                    }
                    Console.WriteLine("Select the ID of the note you want to delete");
                    Int32.TryParse(Console.ReadLine(),out id);
                    pad.DeleteNote(id);
                    break;
                default:
                    Console.WriteLine("Selected: Editing a note");
                    i = 0;
                    foreach (Note note in NoteMemory.Instance.noteList)
                    {
                        Console.WriteLine(i + " " + note.title);
                        i++;
                    }
                    Console.WriteLine("Select the ID of the note you want to edit");
                    Int32.TryParse(Console.ReadLine(), out id);
                    Console.WriteLine("Title:");
                    title = Console.ReadLine();
                    Console.WriteLine("Content:");
                    content = Console.ReadLine();
                    pad.EditNote(title, content, id);
                    break;

            }
        }
    }
}
