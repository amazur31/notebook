﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Notebook
{
    public class NotePad
    {
        NoteListCheck listCheck = new NoteListCheck();
        public void AddNote(string title, string content)
        {
            Note note = new Note();
            note.title = title;
            note.content = content;
            NoteMemory.Instance.noteList.Add(note);   
        }
        public bool DeleteNote(int id)
        {
            if (!listCheck.Exists(id))
            {
                return false;
            }
            else
            {
                return true;
            }        
        }
        public bool EditNote(string title, string content, int id)
        {
            if (!listCheck.Exists(id))
            {
                return false;
            }
            else
            {
                Note note = new Note();
                note.title = title;
                note.content = content;
                NoteMemory.Instance.noteList.RemoveAt(id);
                NoteMemory.Instance.noteList.Add(note);
                return true;
            }
        }
        public void ReadNote()
        {
        }
    }
}
