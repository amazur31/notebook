﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Notebook
{
    public class NoteListCheck
    {

        public bool Exists (int id)
        {
            int i = 0;
            foreach (Note note in NoteMemory.Instance.noteList)
            {
                if (i == id)
                {
                    return true;
                }
                return false;
                i++;
            }
            return false;
        }
    }
}
