﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Notebook
{
    class Program
    {
        static void Main(string[] args)
        {
            bool exit = false;
            while (!exit)
            {
                Console.WriteLine("Currently there is {0} notes", NoteMemory.Instance.noteList.Count);
                Console.WriteLine("Enter 1 for adding a new note, enter 2 for deleting existing note, enter 3 for editing existing note");
                Int32.TryParse(Console.ReadLine(), out int input);
                InputSwitch.GetInput(input);
            }
        }
    }
}
