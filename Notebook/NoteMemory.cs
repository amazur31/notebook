﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Notebook
{
    public sealed class NoteMemory
    {
        public List<Note> noteList = new List<Note>();

        private NoteMemory() { }
        private static NoteMemory instance = null;

        public static NoteMemory Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new NoteMemory();
                }
                return instance;
            }
        }

    }
}
